  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($page == 0) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
    <div style="float:right"><?php if ($picture) {
      print $picture;
    }?></div>
    <?php if ($terms) { ?><p>Tags: <?php print $terms?></p><?php }; ?>
    <div class="content"><?php print $content?></div>
    <div class="details">
    	Posted on <?php print $date ?> by <?php print $name ?>
    	<?php if ($links) { ?><br/>&raquo; <?php print $links?><?php }; ?>
    </div>
  </div>
