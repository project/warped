<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body>
	<div id="header">
		<?php if ($site_name) { ?><h1><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
		<?php if ($site_slogan) { ?><p><?php print $site_slogan ?></p><?php } ?>
	</div>
	<ul id="nav">
		<li class="right"><?php print $search_box ?></li>
		<?php if (isset($primary_links)) { ?> <?php print warped_primary_links() ?> <?php } ?>
	</ul>
	<div class="clear" />
	<?php if ($sidebar_right) { ?>
	<div id="sidebar">
		<?php print $sidebar_right ?>
	</div>
	<?php } ?>
	<div id="content" class="clearfix">
	    <?php if ($mission) { ?> <?php print $mission ?> <?php } ?>
	    <div><?php print $header; ?></div>
	    <h2><?php print $title ?></h2>
	    <div class="tabs"><?php print $tabs ?></div>
	    <?php print $help ?>
	    <?php print $messages ?>
	    <?php print $content; ?>
	</div>
	<div id="footer">
		<?php print $footer_message ?>
    </div>
    <?php print $closure ?>
</body>
</html>