<?php

function phptemplate_search_theme_form($form) {
  return _phptemplate_callback('search-box', array('form' => $form));
}

function warped_primary_links() {
  $output = ''
  $links = menu_primary_links();
  if ($links) {
    foreach ($links as $link) {
      $output .= '<li>'. l($link['title'], $link['href'], 
                  $link['attributes'], $link['query'], 
                  $link['fragment'], FALSE, $link['html']) .'</li>';
    }
  }
  return $output;
}

function phptemplate_comment_thread_collapsed($comment) {
  if ($comment->depth) {
    $output  = '<div style="padding-left:'. ($comment->depth * 25) ."px;\">\n";
    $output .= theme('comment_view', $comment, '', 0);
    $output .= "</div>\n";
  }
  else {
    $output .= theme('comment_view', $comment, '', 0);    
  }
  return $output;
}

function phptemplate_comment_thread_expanded($comment) {
  $output = '';
  if ($comment->depth) {
    $output .= '<div style="padding-left:'. ($comment->depth * 25) ."px;\">\n";
  }

  $output .= theme('comment_view', $comment, module_invoke_all('link', 'comment', $comment, 0));

  if ($comment->depth) {
    $output .= "</div>\n";
  }
  return $output;
}